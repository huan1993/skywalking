# skywalking

#### 介绍
分布式链路追踪 SkyWalking 的学习

#### 安装教程
java -javaagent:/Users/huan/soft/apache-skywalking-apm-bin-es7/agent/skywalking-agent.jar -Dskywalking.agent.service_name=user-service -Dskywalking.collector.backend_service=127.0.0.1:9022 -Dskywalking.trace.ignore_path=/ignoreTrace,/exception -Dskywalking.agent.service_instance=user-service -jar xx.jar  

#### 博客地址  
https://blog.csdn.net/fu_huo_1993/article/details/107188840  

#### 实现功能
```
1、忽略url
2、自定义追踪方法
3、指定实例名
4、追踪子线程的信息
5、全局traceId
6、idea 中的使用
```

#### 微信公众号

`更多内容请关注微信公众号`

![更多内容请关注微信公众号](./images/微信公众号.jpg)