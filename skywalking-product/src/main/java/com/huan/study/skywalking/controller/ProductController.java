package com.huan.study.skywalking.controller;

import com.huan.study.skywalking.domain.Product;
import com.huan.study.skywalking.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author huan.fu 2020/7/5 - 22:54
 */
@RestController
public class ProductController {
    private static final Logger log = LoggerFactory.getLogger(ProductController.class);
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("findAll")
    public Iterable<Product> findAll() {
        log.info("控制层获取所有的商品信息");
        return productService.findAll();
    }
}
