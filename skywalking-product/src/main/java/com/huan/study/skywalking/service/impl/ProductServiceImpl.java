package com.huan.study.skywalking.service.impl;

import com.huan.study.skywalking.domain.Product;
import com.huan.study.skywalking.repository.ProductRepository;
import com.huan.study.skywalking.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author huan.fu 2020/7/5 - 22:51
 */
@Service
public class ProductServiceImpl implements ProductService {
    private static final Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);
    private final ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Iterable<Product> findAll() {
        log.info("服务层获取所有的商品信息");
        return productRepository.findAll();
    }
}
