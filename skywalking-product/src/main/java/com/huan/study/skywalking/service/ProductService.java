package com.huan.study.skywalking.service;

import com.huan.study.skywalking.domain.Product;

/**
 * @author huan.fu 2020/7/5 - 22:51
 */
public interface ProductService {
    Iterable<Product> findAll();
}
