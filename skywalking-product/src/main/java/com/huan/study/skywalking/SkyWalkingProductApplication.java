package com.huan.study.skywalking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author huan.fu 2020/7/6 - 00:21
 */
@SpringBootApplication
public class SkyWalkingProductApplication {
    public static void main(String[] args) {
        SpringApplication.run(SkyWalkingProductApplication.class);
    }
}
