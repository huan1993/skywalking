package com.huan.study.skywalking.repository;

import com.huan.study.skywalking.domain.Product;
import org.springframework.data.repository.CrudRepository;

/**
 * @author huan.fu 2020/7/5 - 22:50
 */
public interface ProductRepository extends CrudRepository<Product, Long> {
}
