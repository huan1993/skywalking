package com.huan.study.skywalking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy
@ServletComponentScan
public class SkywalkingUserApplication {

    public static void main(String[] args) {
        SpringApplication.run(SkywalkingUserApplication.class, args);
    }
}
