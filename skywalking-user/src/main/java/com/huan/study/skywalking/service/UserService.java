package com.huan.study.skywalking.service;

import com.huan.study.skywalking.domain.User;

/**
 * @author huan.fu 2020/7/5 - 22:51
 */
public interface UserService {
    Iterable<User> findAll();
}
