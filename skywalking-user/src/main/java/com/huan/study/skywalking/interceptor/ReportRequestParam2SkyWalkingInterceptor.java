package com.huan.study.skywalking.interceptor;

import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.logging.Log;
import org.springframework.aop.interceptor.AbstractTraceInterceptor;
import org.springframework.stereotype.Component;

/**
 * @author huan.fu 2020/7/9 - 18:59
 */
@Component
public class ReportRequestParam2SkyWalkingInterceptor extends AbstractTraceInterceptor {
    @Override
    protected Object invokeUnderTrace(MethodInvocation methodInvocation, Log log) throws Throwable {
        return methodInvocation.proceed();
    }
}
