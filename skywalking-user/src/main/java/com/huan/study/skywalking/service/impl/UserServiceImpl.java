package com.huan.study.skywalking.service.impl;

import com.huan.study.skywalking.domain.User;
import com.huan.study.skywalking.repository.UserRepository;
import com.huan.study.skywalking.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author huan.fu 2020/7/5 - 22:51
 */
@Service
public class UserServiceImpl implements UserService {
    private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Iterable<User> findAll() {
        log.info("服务层获取所有的用户信息");
        return userRepository.findAll();
    }
}
