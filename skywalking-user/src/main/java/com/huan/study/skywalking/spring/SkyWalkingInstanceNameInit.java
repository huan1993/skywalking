package com.huan.study.skywalking.spring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.core.Ordered;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MapPropertySource;

import java.util.HashMap;
import java.util.Map;

/**
 * @author huan.fu 2020/7/17 - 14:00
 */
public class SkyWalkingInstanceNameInit implements EnvironmentPostProcessor, Ordered {
    private static final Logger log = LoggerFactory.getLogger(SkyWalkingInstanceNameInit.class);

    @Override
    public int getOrder() {
        return 0;
    }

    @Override
    public void postProcessEnvironment(ConfigurableEnvironment environment, SpringApplication application) {
        log.info("系统环境初始化完成");
        Map<String, Object> source = new HashMap<>(4);
        source.put("server.port", "9088");
        MapPropertySource propertySource = new MapPropertySource("rewriteConfigProperties", source);
        // 放到首位
        environment.getPropertySources().addFirst(propertySource);
    }
}
